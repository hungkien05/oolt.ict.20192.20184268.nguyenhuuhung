package OtherProject.hust.soict.garbage;
import java.util.Random;

public class GarbageCreator{
    //Create “garbage” as much as possible 
    //You may not want to run this !
    public static void main(String[] args) {
        Random r = new Random(123);
        long start = System.currentTimeMillis();
        String s = "";
        for (long i =0; i<1000000000; i++)
            s += r.nextInt(2);
        // System.out.println(s);
        System.out.println(System.currentTimeMillis() - start);
    }
}