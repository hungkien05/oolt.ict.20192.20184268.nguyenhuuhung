package AimsProject.hust.soict.ictglobal.aims;

import java.util.Scanner;

public class Aims {
    public static void main(String[] args) {
        Order anOrder = new Order();
        DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
        dvd1.setCategory("Animation");
        dvd1.setCost(19.95f);
        dvd1.setDirector("Roger Allers");
        dvd1.setLength(87);
        //add the dvd to order
        anOrder.addDigitalVideoDisc(dvd1);

        DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
        dvd2.setCategory("Science FIction");
        dvd2.setCost(24.95f);
        dvd2.setDirector("George Lucas");
        dvd2.setLength(124);
        anOrder.addDigitalVideoDisc(dvd2);

        DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
        dvd3.setCategory("Animation");
        dvd3.setCost(18.99f);
        dvd3.setDirector("John Musker");
        dvd3.setLength(90);
        anOrder.addDigitalVideoDisc(dvd3);

        System.out.println("Total cost is: " + anOrder.totalCost());
        
        
        System.out.println("Test lab05 methods...");
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Enter a string for  boolean searching: "); // recommend enter "tHe kINg" for testing purpose
        String s = keyboard.nextLine();
        keyboard.close();
        boolean kt=false;
        for (int i = 1; i <= anOrder.getQtyOrdered(); i++) {
            if (anOrder.itemsOrdered[i].booleanSearch(s) ) {
                System.out.println("Found a title: " + anOrder.itemsOrdered[i].getTitle() ); 
                kt=true;
            }
            
        }
        if (kt==false) System.out.println("Nothing found");
        // System.out.println(dvd1.booleanSearch("The King"));
        System.out.println("Getting your lucky item...");
        DigitalVideoDisc luckydvd;
        luckydvd = anOrder.getALuckyItem();
        luckydvd.setCost(0); // because its free
        System.out.println("Here is your lucky free item: "+ luckydvd.getTitle());
        anOrder.addDigitalVideoDisc(luckydvd);
    }
}