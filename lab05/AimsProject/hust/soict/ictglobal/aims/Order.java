package AimsProject.hust.soict.ictglobal.aims;

import java.util.Random;

public class Order {
    public static final int MAX_NUMBERS_ORDERED = 10;
    public DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED+1];
    private int qtyOrdered;
    float total=0;

    public int getQtyOrdered() {
        return qtyOrdered;
    }

    public void setQtyOrdered(int qtyOrdered) {
        this.qtyOrdered = qtyOrdered;
    }
    public void addDigitalVideoDisc(DigitalVideoDisc disc) {
        if (qtyOrdered<10) {
            this.qtyOrdered+=1;
            this.itemsOrdered[qtyOrdered]= disc;
            System.out.println("The disc has been added");
        } else {
            System.out.println("The order is full");
        }
    }
    public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
        
        this.itemsOrdered[qtyOrdered]= null;
        this.qtyOrdered-=1;
        System.out.println("The disc has been removed");
    }
    public float totalCost(){
        for (int i = 1; i <= qtyOrdered; i++) {
            total += itemsOrdered[i].getCost();
        }
        return total;
    }
    public DigitalVideoDisc getALuckyItem(){
        Random rd = new Random();
        int k= rd.nextInt(qtyOrdered)+1;
        return itemsOrdered[k];
    }
}