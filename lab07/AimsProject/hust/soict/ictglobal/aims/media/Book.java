package AimsProject.hust.soict.ictglobal.aims.media;

import java.util.ArrayList;
import java.util.List;

public class Book extends Media {
    private List<String> authors = new ArrayList<String>();
    int b;

    public Book(){
        b=1;
    }

    public Book(String title){
        super(title);
        // this.title = title;
        b=1;
    }
    public Book(String title, String category) {
        super(title, category);
    }
    public Book(String title, String category, float cost) {
        super(title, category,cost);
    }
    public Book(String title, String category, List<String> authors ) {
        super(title, category);
        this.authors = authors;        
    }


    public List<String> getAuthors() {
        return authors;
    }

    public void setAuthors(List<String> authors) {
        this.authors = authors;
    }

    public void addAuthor(String authorName) {
        if (this.authors.contains(authorName)) {
            System.out.println("Author's name already exists");
        } else {
            this.authors.add(authorName);
        }
    }

    public void removeAuthor(String authorName) {
        if (this.authors.contains(authorName)) {
            this.authors.remove(authorName);
        } else {
            System.out.println("Author's name not exist");
        }
    }
}