package AimsProject.hust.soict.ictglobal.aims.media;

public class Disc extends Media {
    int length;
    String director;

    public int getLength() {
        return length;
    }

    public String getDirector() {
        return director;
    }

    public Disc(String title) {
        super(title);
    }
    public Disc(String title, String category) {
        super(title,category);
    }
    public Disc(String title, String category, float cost) {
        super(title,category,cost);
    }
    public Disc(int length) {
        this.length= length;
    }
    public Disc(int length, String director){
        this.length = length;
        this.director = director;
    }
    public Disc(String title, String category, float cost, String director,  int length){
        super(title,category,cost);
        this.length = length;
        this.director = director;
    }
}