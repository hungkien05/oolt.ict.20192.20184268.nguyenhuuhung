package AimsProject.hust.soict.ictglobal.aims.media;

import java.util.ArrayList;

public class CompactDisc extends Disc implements Playable {
    private String artist;
    private int length;
    private ArrayList<Track> tracks = new ArrayList<>();

    public CompactDisc(String title){
        super(title);
    }
    public CompactDisc(String title, String category, float cost, String director, int length) {
        super(title, category, cost, director, length);
        
    }

    public CompactDisc(String title, String category, float cost) {
        super(title, category,cost);
    }

    public String getArtist() {
        return artist;
    }
    public int getLength() {
        int totalLength=0;
        for (int i = 0; i < tracks.size(); i++) {
            totalLength +=tracks.get(i).getLength();
        }
        length = totalLength;
        return length;
    }

    public void addAuthor(Track trackName) {
        if (this.tracks.contains(trackName)) {
            System.out.println("This track already exists");
        } else {
            this.tracks.add(trackName);
        }
    }

    public void removeAuthor(Track trackName) {
        if (this.tracks.contains(trackName)) {
            this.tracks.remove(trackName);
        } else {
            System.out.println("This track not exist");
        }
    }

    @Override
    public void play() {
        System.out.println("Playing compact disc " + this.getTitle() + " of artist: " + this.getArtist());
        for (int i = 0; i < tracks.size(); i++) {
            // System.out.println("Track "+ i 
            // +" - Title: "+tracks.get(i).getTitle() + " Length: " + tracks.get(i).getLength());
            tracks.get(i).play();
        }
    }

}