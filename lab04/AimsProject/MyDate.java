public class MyDate {
    private int day,month,year;
    String s;

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public MyDate() {
    }
    public MyDate(String s) {
        this.s=s;
        this.accept(s);
    }
    public MyDate(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }
    public void accept(String s){
        String[] tokens = s.split(" ");
        switch (tokens[0]) {
            case "January":
                this.month=1;
                break;
            case "February":
                this.month=2;
                break;
            case "March":
                this.month=3;
                break;
            case "April":
                this.month=4;
                break;
            case "May":
                this.month=5;
                break;
            case "June":
                this.month=6;
                break;
            case "July":
                this.month=7;
                break;
            case "August":
                this.month=8;
                break;
            case "September":
                this.month=9;
                break;
            case "October":
                this.month=10;
                break;
            case "November":
                this.month=11;
                break;
            default:
                this.month=12;
                break;
        }
        this.day = Integer.parseInt(tokens[1]);
        this.year = Integer.parseInt(tokens[2]);
    }
    public void print() {
        System.out.println("The date is: " + this.day +"-" + this.month + "-" +this.year);
    }
    
    public static void main(String[] args) {
        //test 3 examples on 3 methods
        MyDate date1 = new MyDate();
        date1.accept("October 4 2000");
        date1.print();
        MyDate date2 = new MyDate(23,8,2000);
        date2.print();
        MyDate date3 = new MyDate("February 18 2019");
        date3.print();
    }
}