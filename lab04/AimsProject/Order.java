public class Order {
    public static final int MAX_NUMBERS_ORDERED = 10;
    public static final int MAX_LIMITED_ORDERS = 5;
    private static int nbOrders =0;
    private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED+1];
    private int qtyOrdered;
    float total=0;
    private int dateOrdered;

    public int getQtyOrdered() {
        return qtyOrdered;
    }

    public void setQtyOrdered(int qtyOrdered) {
        this.qtyOrdered = qtyOrdered;
    }
    public int getDateOrdered() {
        return dateOrdered;
    }

    public void setDateOrdered(int dateOrdered) {
        this.dateOrdered = dateOrdered;
    }

    public void addDigitalVideoDisc(DigitalVideoDisc disc) {
        if (qtyOrdered<10) {
            this.qtyOrdered+=1;
            this.itemsOrdered[qtyOrdered]= disc;
            System.out.println("The disc has been added");
        } else {
            System.out.println("The order is full");
        }
    }
    public void addDigitalVideoDisc(DigitalVideoDisc [] dvdList){
        int i=0;
        if (qtyOrdered+dvdList.length>10) {
            System.out.println("The order cannot hold your list");
            return;
        }
        while (i<dvdList.length) {
            this.qtyOrdered+=1;
            this.itemsOrdered[qtyOrdered]= dvdList[i];
            i=i+1;
            System.out.println("The disc has been added");
            // System.out.println(qtyOrdered + "  " + i);
        } 
    }
    public void addDigitalVideoDisc(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2){
        if (qtyOrdered+2>10) {
            System.out.println("The order cannot hold your list");
            return;
        }
        
        this.qtyOrdered+=1;
        this.itemsOrdered[qtyOrdered]= dvd1;
        System.out.println("The disc has been added");
        this.qtyOrdered+=1;
        this.itemsOrdered[qtyOrdered]= dvd2;
        System.out.println("The disc has been added");
        // System.out.println(qtyOrdered + "  " + i);
        
    }
    public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
        this.itemsOrdered[qtyOrdered]= null;
        this.qtyOrdered-=1;
        System.out.println("The disc has been removed");
    }
    public float totalCost(){
        for (int i = 1; i <= qtyOrdered; i++) {
            total += itemsOrdered[i].getCost();
        }
        return total;
    }

    public Order(int dateOrdered) {
        this.dateOrdered = dateOrdered;
        nbOrders +=1;
        if (nbOrders >=MAX_LIMITED_ORDERS) System.out.println("Exceed limited number of orders");

    }
    public Order() {
        nbOrders +=1;
        if (nbOrders >=MAX_LIMITED_ORDERS) System.out.println("Exceed limited number of orders");

    }

    public void printOrder() {
        System.out.println("Date: "+ this.dateOrdered);
        System.out.println("Ordered items:");
        for (int i = 1; i <= qtyOrdered; i++) {
             System.out.println(i + ".DVD - " + itemsOrdered[i].getTitle() + " - " 
            + itemsOrdered[i].getCategory() + " - " 
            + itemsOrdered[i].getDirector() + " - " 
            + itemsOrdered[i].getDirector() + " - " 
            + itemsOrdered[i].getLength() + " - " 
            + itemsOrdered[i].getCost() );
        }
    }
}