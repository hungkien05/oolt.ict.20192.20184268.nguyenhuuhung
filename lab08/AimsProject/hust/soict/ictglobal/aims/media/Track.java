package AimsProject.hust.soict.ictglobal.aims.media;

public class Track implements Playable, Comparable{
    private String title;
    private int length;

    public Track(String title) {
        this.title = title;
    }

    public Track(String title, int length) {
        this.title = title;
        this.length = length;
    }

    public String getTitle() {
        return title;
    }
    public int getLength() {
        return length;
    }

    @Override
    public void play() {
        System.out.println("Playing DVD: " + this.getTitle());
        System.out.println("DVD length: " + this.getLength());
    }
    @Override
    public boolean equals(Object obj) {
        Track track= (Track)obj;
        
        return ( this.title.equals(track.getTitle()) && this.length == track.getLength() );
    }

    @Override
    public int compareTo(Object o) {
        Track x = (Track) o;
        return this.title.compareTo(x.getTitle());
    }
    
}