package AimsProject.hust.soict.ictglobal.aims.media;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class TestMediaCompareTo {
    public static void main(String[] args) {
        DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King","Animation", 19.95f, "Roger Allers", 87);
        DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars", "Science Fiction", 24.95f, "George Lucas", 124);
        DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin","Animation", 18.99f, "John Musker", 90);
        Book book1 = new Book("Mat biec", "Romance", 11f);
        book1.addAuthor("Nguyen Nhat Anh");
        Collection collection = new ArrayList();
        collection.add(dvd2);
        collection.add(dvd1);
        collection.add(dvd3);

        Iterator iterator = collection.iterator();
        System.out.println("------------\nThe DVDs in the collection are: ");

        while (iterator.hasNext()){
            System.out.println( ((DigitalVideoDisc)iterator.next() ).getTitle() );
        }
        System.out.println("------------");
        Collections.sort((List) collection);
        iterator = collection.iterator();
        System.out.println("------------\nThe DVDs in the sorted collection are: ");

        while (iterator.hasNext()){
            System.out.println( ((DigitalVideoDisc)iterator.next() ).getTitle() );
        }
    }

}