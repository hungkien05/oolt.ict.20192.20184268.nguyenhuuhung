package AimsProject.hust.soict.ictglobal.aims;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import AimsProject.hust.soict.ictglobal.aims.media.*;
public class Aims {
    public static void showMenu(Scanner sc) {
        System.out.println("Order Management Application: ");
        System.out.println("--------------------------------");
        System.out.println("1. Create new order");
        System.out.println("2. Add item to the order");
        System.out.println("3. Delete item by id");
        System.out.println("4. Display the items list of order");
        System.out.println("5. Test compare 2 given CDs based on num of tracks and length: ");
        System.out.println("0. Exit");
        System.out.println("--------------------------------");
        System.out.println("Please choose a number: 0-1-2-3-4-5");       
        Order order = new Order();
        int id =-1;
        Scanner keyboard1 = new Scanner(System.in);
        Scanner keyboard2 = new Scanner(System.in);

        while (id != 0){ 
            
            // Scanner keyboard = new Scanner(System.in);
            int x = sc.nextInt();
            id =x;
            
            // keyboard.close();
            switch (id) {
                case 1:
                    System.out.println("New order has been created");
                    break;
                case 2:
                    System.out.print("Enter your type of item: (enter one of these: Book,CompactDisc or DigitalVideoDisc ):  ");
                    
                    String type = keyboard1.nextLine();
                    System.out.print("Enter your title:");
                    String title = keyboard1.nextLine();
                    if (type.equals("Book")) {
                        Book newItem = new Book(title);
                        order.addMedia(newItem);
                    }
                    if (type.equals("CompactDisc")) {
                        CompactDisc newItem = new CompactDisc(title);
                        order.addMedia(newItem);
                        System.out.print("Do you want to play this cd: (y/n) ");
                        String s = keyboard1.nextLine();
                        if (s.equals("y")) {
                            newItem.play();
                        }
                    }
                    if (type.equals("DigitalVideoDisc")) {
                        DigitalVideoDisc newItem = new DigitalVideoDisc(title);
                        order.addMedia(newItem);
                        System.out.print("Do you want to play this cd: (y/n) ");
                        String s = keyboard1.nextLine();
                        if (s.equals("y")) {
                            newItem.play();
                        }
                    }
                    // order.addMedia(newItem);
                    
                    break;
                case 3:
                    System.out.println("Enter the id you want to remove: ");
                    
                    int k = keyboard2.nextInt();
                    System.out.println(k);
                    if (k> order.itemsOrdered.size()-1) {
                        System.out.println("Invalid id");
                    } else {
                        order.itemsOrdered.remove(k);
                        System.out.println("Item" + id + "has been removed");
                    }
                    break;
                case 4:
                System.out.print("Here is your order: ");
                    for (int i = 0; i < order.itemsOrdered.size(); i++) {
                        System.out.print(", " + order.itemsOrdered.get(i).getTitle());
                    }
                    System.out.println(" ");
                    break;
                case 5:
                    Track t1= new Track("t1",1);
                    Track t2= new Track("t2",2);
                    Track t3= new Track("t3",3);
                    CompactDisc cd1 = new CompactDisc("cd1");
                    cd1.addTrack(t3);
                    cd1.addTrack(t2);
                    CompactDisc cd2 = new CompactDisc("cd2");
                    cd2.addTrack(t1);
                    cd2.addTrack(t3);
                    List<CompactDisc> collection = new ArrayList<CompactDisc>();
                    collection.add(cd1);
                    collection.add(cd2);
                    CompactDisc tmpcd;

                    System.out.println("------------\nThe DVDs in the original collection are: ");
                    Iterator iterator = collection.iterator();
                    while (iterator.hasNext()){
                        tmpcd = (CompactDisc)iterator.next() ;
                        System.out.println( tmpcd.getTitle() + " with  " + tmpcd.tracks.size() + " track(s)" 
                        + ", length = " + tmpcd.getLength());
                    }

                    Collections.sort( (List) collection);
                    iterator = collection.iterator();
                    System.out.println("------------\nThe DVDs in the sorted collection are: ");

                    while (iterator.hasNext()){
                        System.out.println( ((CompactDisc)iterator.next() ).getTitle() );
                    }
                    break;

                case 0:
                   break;
                default:
            
                    break;
            }
            
        }
        keyboard1.close();
        keyboard2.close();
    }

    public static void main(String[] args) {
        System.out.println("An example by programmer for testing purpose: ");
        Order anOrder = new Order();
        DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King","Animation", 19.95f, "Roger Allers", 87);
        //add the dvd to order
        anOrder.addMedia(dvd1);

        DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars", "Science Fiction", 24.95f, "George Lucas", 124);
        anOrder.addMedia(dvd2);

        DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin","Animation", 18.99f, "John Musker", 90);
        anOrder.addMedia(dvd3);

        Book book1 = new Book("Mat biec", "Romance", 11f);
        book1.addAuthor("Nguyen Ngoc Anh");
        anOrder.addMedia(book1);

        System.out.println("Total cost is: " + anOrder.totalCost());
        System.out.println("-----------");
        Scanner sc = new Scanner(System.in);
        showMenu(sc);
        sc.close();
        
        
    }
}