package AimsProject.hust.soict.ictglobal.aims;

import java.util.ArrayList;
import java.util.Random;
import AimsProject.hust.soict.ictglobal.aims.media.Media;

public class Order {
    public static final int MAX_NUMBERS_ORDERED = 10;
    ArrayList<Media> itemsOrdered = new ArrayList<Media>();
    private int qtyOrdered;
    float total=0;
    public Order(){

    }

    public void addMedia(Media media) {
        this.itemsOrdered.add(media);
        System.out.println("The media has been added");
    }
    public void removeMedia(Media media){
        this.itemsOrdered.remove(media);
        System.out.println("The media has been removed");
    }

    public float totalCost(){
        for (int i = 0; i <itemsOrdered.size(); i++) {
            total += itemsOrdered.get(i).getCost();
        }
        return total;
    }
    public Media getALuckyItem(){
        Random rd = new Random();
        int k= rd.nextInt(qtyOrdered)+1;
        return itemsOrdered.get(k);
    }
}