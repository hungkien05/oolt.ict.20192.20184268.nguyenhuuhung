package AimsProject.hust.soict.ictglobal.aims.media;

import java.util.ArrayList;

public class CompactDisc extends Disc implements Playable, Comparable {
    private String artist;
    private int length;
    public ArrayList<Track> tracks = new ArrayList<>();

    public CompactDisc(String title){
        super(title);
    }
    public CompactDisc(String title, int length){
        super(title,length);

    }
    public CompactDisc(String title, String category, float cost, String director, int length) {
        super(title, category, cost, director, length);
        
    }

    public CompactDisc(String title, String category, float cost) {
        super(title, category,cost);
    }

    public String getArtist() {
        return artist;
    }
    public int getLength() {
        int totalLength=0;
        for (int i = 0; i < tracks.size(); i++) {
            totalLength +=tracks.get(i).getLength();
        }
        this.length = totalLength;
        return length;
    }

    public void addTrack(Track trackName) {
        if (this.tracks.contains(trackName)) {
            System.out.println("This track already exists");
        } else {
            this.tracks.add(trackName);
        }
    }

    public void removeTrack(Track trackName) {
        if (this.tracks.contains(trackName)) {
            this.tracks.remove(trackName);
        } else {
            System.out.println("This track not exist");
        }
    }

    @Override
    public void play() {
        System.out.println("Playing compact disc " + this.getTitle() + " of artist: " + this.getArtist());
        for (int i = 0; i < tracks.size(); i++) {
            // System.out.println("Track "+ i 
            // +" - Title: "+tracks.get(i).getTitle() + " Length: " + tracks.get(i).getLength());
            tracks.get(i).play();
        }
    }

    @Override
    public int compareTo(Object o) {
        CompactDisc x = (CompactDisc) o;
        if (this.tracks.size()!= x.tracks.size() ){
            System.out.println("so track khac nhau");
            return this.tracks.size() - x.tracks.size();
        } else {
            return this.length-x.length;
        }
    }
}