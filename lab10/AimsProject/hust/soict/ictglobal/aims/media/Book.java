package AimsProject.hust.soict.ictglobal.aims.media;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class Book extends Media implements Comparable{
    private List<String> authors = new ArrayList<String>();
    int b;
    String content;
    List<String> contentTokens;
    Map<String,Integer> wordFrequency;

    public Book(){
        b=1;
    }

    public Book(String title){
        super(title);
        // this.title = title;
        b=1;
    }

    public Book(String title, String content) {
        super(title);
        this.content=content;
    }
    public Book(String title, String category, float cost) {
        super(title, category,cost);
    }
    public Book(String title, String category, List<String> authors ) {
        super(title, category);
        this.authors = authors;        
    }


    public List<String> getAuthors() {
        return authors;
    }

    public void setAuthors(List<String> authors) {
        this.authors = authors;
    }

    public void addAuthor(String authorName) {
        if (this.authors.contains(authorName)) {
            System.out.println("Author's name already exists");
        } else {
            this.authors.add(authorName);
        }
    }

    public void removeAuthor(String authorName) {
        if (this.authors.contains(authorName)) {
            this.authors.remove(authorName);
        } else {
            System.out.println("Author's name not exist");
        }
    }

    @Override
    public int compareTo(Object o) {
        Book x = (Book) o;
        return this.getTitle().compareTo(x.getTitle());
    }

    public void processContent(){
        String token[] = this.content.split(" ");
        for (int i = 0; i < token.length; i++) {
            contentTokens.add(token[i]);
        }
        Collections.sort(contentTokens);
    }
}