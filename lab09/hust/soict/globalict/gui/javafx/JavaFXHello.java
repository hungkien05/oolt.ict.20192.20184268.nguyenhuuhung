package sample;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
public class JavaFXHello extends Application {
    private  Button btnhello;
    private Button btnhello1;

    @Override
    public void start(Stage primaryStage) {
        btnhello = new Button();
        btnhello.setText("Say Hello");
        btnhello.setOnAction(evt -> System.out.println("Hello world"));
        StackPane root = new StackPane();
        root.getChildren().add(btnhello);
//        root.getChildren().add(btnhello1);
        Scene scene = new Scene(root, 300 ,100);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Hello");
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
