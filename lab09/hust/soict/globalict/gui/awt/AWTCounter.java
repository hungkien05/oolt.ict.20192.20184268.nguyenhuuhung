package hust.soict.globalict.gui.awt;
import java.awt.*;
// import java.awt.Frame;
import java.awt.event.*;

public class AWTCounter extends Frame implements ActionListener {
    private Label lblCount;
    private TextField tfCount;
    private Button btnCount;
    private int count = 0;

    public AWTCounter(){
        setLayout(new FlowLayout()); 
        lblCount = new Label("Counter");
        add(lblCount);
        tfCount = new TextField(count +"", 10);
        tfCount.setEditable(false);
        add(tfCount);
        btnCount = new Button("Count");
        add(btnCount);
        btnCount.addActionListener(this);
        setTitle("AWT Counter");
        setSize(250,100);
        // For inspecting the Container/Components objects
        // System.out.println(this);
        // System.out.println(lblCount);
        //
        System.out.println(tfCount);
        // System.out.println(btnCount);
        setVisible(true);
        this.addWindowListener(
            new WindowAdapter(){
                public void windowClosed(WindowEvent e) {System.exit(0);}

            }
        );
        // "super" Frame shows
        // System.out.println(this);
        // System.out.println(lblCount);
        // System.out.println(tfCount);
        //System.out.println(btnCount);
    }
    public static void main(String[] args) {
        AWTCounter app = new AWTCounter();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        count++;
        tfCount.setText(count+""); //convert to string
    }
}