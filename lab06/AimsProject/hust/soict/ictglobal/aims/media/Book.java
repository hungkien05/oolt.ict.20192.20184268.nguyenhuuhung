package AimsProject.hust.soict.ictglobal.aims.media;

import java.util.ArrayList;
import java.util.List;

public class Book extends Media {
    private List<String> authors = new ArrayList<String>();
    int b;

    public Book(){
        b=1;
    }

    public Book(String title){
        super(title);
        // this.title = title;
        b=1;
    }
    public Book(String title, String category) {
        super(title, category);
        // TODO Auto-generated constructor stub
    }
    public Book(String title, String category, float cost) {
        super(title, category);
        this.setCost(cost);
        // TODO Auto-generated constructor stub
    }
    public Book(String title, String category, List<String> authors ) {
        super(title, category);
        this.authors = authors;
        // TODO Auto-generated constructor stub
    }


    public List<String> getAuthors() {
        return authors;
    }

    public void setAuthors(List<String> authors) {
        this.authors = authors;
    }

    public void addAuthor(String authorName) {
        if (this.authors.contains(authorName)) {
            System.out.println("Author's name already exists");
        } else {
            this.authors.add(authorName);
        }
    }

    public void removeAuthor(String authorName) {
        if (this.authors.contains(authorName)) {
            this.authors.remove(authorName);
        } else {
            System.out.println("Author's name not exist");
        }
    }
    public static void main(String[] args) {
        Media m1 = new Media();
        Book b1 = new Book();
        // m1=b1;
        Media am[]={m1,b1};
    
        ((Book) m1).addAuthor("Hung");
        System.out.println();
    }
}