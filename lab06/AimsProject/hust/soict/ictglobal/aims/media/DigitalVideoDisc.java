package AimsProject.hust.soict.ictglobal.aims.media;

public class DigitalVideoDisc extends Media {
    
    private String director;
    private int length;

    //Setters and getter
    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }
    //Constructors
    public DigitalVideoDisc(String title) {
        super(title);
    }

    public DigitalVideoDisc(String title, String category) {
        super(title, category);
    }
    
    public DigitalVideoDisc(String title, String category, String director) {
        super(title, category);
        this.director = director;
    }

    public DigitalVideoDisc(String title, String category, String director, int length, float cost) {
        super(title, category);
        this.director = director;
        this.length = length;
        this.setCost(cost);
    }
    public boolean booleanSearch(String t){
        int dem=0;
        String[] token1 = t.split(" ");
        String[] token2 = this.getTitle().split(" ");
        for (int i = 0; i < token1.length; i++) {
            for (int j = 0; j < token2.length; j++) {
                if ( token1[i].equalsIgnoreCase(token2[j]) ) {
                    // System.out.println(token1[i] + " " + token2[j] + " " + dem);
                    dem++;
                    break;
                }
            }
        }
        if (dem == token1.length) return true;
        return false;
    }
}