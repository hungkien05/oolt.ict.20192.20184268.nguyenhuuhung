package lab02;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class lab2_exer6 {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Enter number of elements: ");
        int n = keyboard.nextInt();
        int sum=0;
        ArrayList<Integer> a = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            int k = keyboard.nextInt();
            a.add(k);
            sum+=k;
        }
        Collections.sort(a);
        System.out.print("Sorted array: ");
        for (int i = 0; i < n; i++) {
            System.out.print(a.get(i) + " ");
        }
        System.out.println("Sum of array: "+ sum);
        System.out.println("Average value of array: "+ (double)sum/n);
        keyboard.close();
    }
}