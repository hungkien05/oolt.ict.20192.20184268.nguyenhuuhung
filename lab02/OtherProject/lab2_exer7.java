package lab02;
import java.util.Scanner;

public class lab2_exer7 {
    public static void main(final String[] args){
        System.out.print("Enter the number of row and column: ");
        Scanner keyboard = new Scanner(System.in);
        int r = keyboard.nextInt();
        int c = keyboard.nextInt();
        int a[][]= new int[r][c];
        int b[][]= new int[r][c];
        System.out.println("Enter the first matrix: ");
        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++) {
                a[i][j]= keyboard.nextInt();
            }
        }
        System.out.println("Enter the second matrix: ");
        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++) {
                b[i][j]= keyboard.nextInt();
            }
        }
        // int a[][]={{1,2,3},{4,5,6},{7,8,9}};
        // int b[][]={{1,1,1},{1,1,1},{1,1,1}};
        System.out.println("Here is the sum of the 2 matrices: ");
        int x[][]=new int[r][c];
        for(int i = 0;i<r;i++){
            for(int j = 0;j<c;j++){
                x[i][j] = a[i][j]+b[i][j];
                System.out.print(x[i][j]+" ");
            }
            System.out.println();
        }
        
    }
}