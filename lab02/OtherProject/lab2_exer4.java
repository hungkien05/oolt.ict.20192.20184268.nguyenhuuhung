package lab02;
import java.util.Scanner;
public class lab2_exer4{
    public static void main(String[] args) {
        System.out.print("Please enter n:");
        Scanner keyboard = new Scanner(System.in);
        int n = keyboard.nextInt();
        for (int i = 1; i <= n; i++) {
            int j;
            for ( j = 1; j <= n-i; j++) {
                System.out.print(" ");
            }
            for ( j = 1; j <= i*2-1; j++) {
                System.out.print("*");
            }
            for ( j = 1; j <= n-i; j++) {
                System.out.print(" ");
            }
            System.out.println("");
        }
        keyboard.close();
    }
}