package lab02;
import java.util.Scanner;

public class lab2_exer5 {
    public static void main(final String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Enter the month: ");
        int m = keyboard.nextInt();
        while (m<1 || m>12) {
            System.out.print("Ivalid! Enter again the month: ");
            m = keyboard.nextInt();
        }
        System.out.print("Enter the year: ");
        int y = keyboard.nextInt();
        if (m==1 || m==3 || m==5 || m==7 || m==8 || m==10 || m==12) 
            System.out.print("31 days");
        if (m==4 || m==6 || m==9 || m==11) 
            System.out.print("30 days");
        if (m==2 && y%4==0) 
            System.out.print("29 days");
        if (m==2 && y%4!=0) 
            System.out.print("28 days");
        keyboard.close();
    }
    
}