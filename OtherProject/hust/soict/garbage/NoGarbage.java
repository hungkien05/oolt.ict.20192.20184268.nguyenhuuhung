package OtherProject.hust.soict.garbage;
import java.util.*;
public class NoGarbage {
    public static void main(String[] args) {
    //use StringBuilder to not create too much garbage

    Random r = new Random(123);
    long start = System.currentTimeMillis();
    StringBuilder sb = new StringBuilder();
    for (int i=0; i<65536;i++)
        sb.append(r.nextInt(2));
    String s = sb.toString();
    System.out.println(System.currentTimeMillis()- start);
    }
}    